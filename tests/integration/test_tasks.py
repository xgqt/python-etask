#!/usr/bin/env python3


"""
Test of the "emacs" module.
"""  """

This file is part of python-etask - CLI interface to GNU Emacs in Python.
Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
SPDX-License-Identifier: GPL-2.0-or-later

python-etask is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

python-etask is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-etask.  If not, see <https://www.gnu.org/licenses/>.
"""


import os

from etask.tasks import Tasks
from etask.pprint import Pprint


EL_TEST_DIR = os.path.join(os.path.dirname(__file__), "elisp")


def test_elisp_compile_none():
    """
    Test if compiling nothing does not produce any errors.
    """

    assert Tasks(Pprint(False)).elisp_compile([]) is None


def test_elisp_compile():
    """
    Compile a simple ELisp file.
    """

    el_test_file = os.path.join(EL_TEST_DIR, "test_elisp_compile.el")
    el_test_file_compiled = el_test_file + "c"

    # Clean before compilation
    if os.path.exists(el_test_file_compiled):
        os.remove(el_test_file_compiled)

    Tasks(Pprint(False)).elisp_compile([el_test_file])

    assert os.path.exists(el_test_file_compiled)

    os.remove(el_test_file_compiled)


def test_elisp_compile_fail(capsys):
    """
    Attempt to compile a simple ELisp file (wants failure).
    """

    el_test_file = os.path.join(EL_TEST_DIR, "test_elisp_compile_fail.el")

    Tasks(Pprint(False)).elisp_compile([el_test_file])

    wanted_stdout = "Error: End of file during parsing"
    caught_stdout = capsys.readouterr().out

    assert wanted_stdout in caught_stdout


def test_elsip_eval_nocheck():
    """
    Test if evaluating a simple expression does not produce any errors.
    """

    expression_string = "(princ \"test_elsip_eval_nocheck\")"
    Tasks(Pprint(False)).elisp_eval(expression_string)
