#!/usr/bin/env python3


"""
Test of the "emacs" module.
"""  """

This file is part of python-etask - CLI interface to GNU Emacs in Python.
Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
SPDX-License-Identifier: GPL-2.0-or-later

python-etask is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

python-etask is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-etask.  If not, see <https://www.gnu.org/licenses/>.
"""


from etask.emacs import Emacs
from etask.pprint import Pprint


def test_emacs_executable_found():
    """
    Test if Emacs is found.
    """

    assert Emacs(Pprint(False)).executable_path


def test_no_output():
    """
    Tests that nothing produces no output.
    """

    std_lines = Emacs(Pprint(False)).execute()

    assert std_lines["stderr"] == [""]
    assert std_lines["stdout"] == [""]


def test_princ_stdout():
    """
    Tests that "princ" uses std out.
    """

    std_lines = Emacs(Pprint(False)).execute("--eval", "(princ \"princ\")")

    assert std_lines["stderr"] == [""]
    assert std_lines["stdout"] == ["princ"]


def test_message_stderr():
    """
    Tests that "message" uses std error.
    """

    std_lines = Emacs(Pprint(False)).execute("--eval", "(message \"message\")")

    assert std_lines["stderr"] == ["message", ""]
    assert std_lines["stdout"] == [""]


def test_emacs_version():
    """
    Test if Emacs version is higher than 23.1.
    """

    assert float(Emacs(Pprint(False)).get_version()) >= 23.1


def test_eval_princ_one():
    """
    Tests if eval_princ returns 1.
    """

    assert Emacs(Pprint(False)).eval_princ("1") == ["1"]


def test_eval_two_plus_two():
    """
    Test if eval returns 4.
    """

    assert Emacs(Pprint(False)).eval("(princ (+ 2 2))") == ["4"]


def test_eval_princ_two_plus_two():
    """
    Test if eval_princ returns 4.
    """

    assert Emacs(Pprint(False)).eval_princ("(+ 2 2)") == ["4"]


def test_extra_kill():
    """
    Kill Emacs using "extra".
    """

    out = Emacs(Pprint(False), extra=["--eval", "(kill-emacs)"]).execute()

    assert out == {"stderr": [""], "stdout": [""]}


def test_package_installed_p():
    """
    Test if package "package" is installed.
    """

    assert Emacs(Pprint(False)).package_installed_p("package")
