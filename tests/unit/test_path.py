#!/usr/bin/env python3


"""
Test of the "path" module.
"""  """

This file is part of python-etask - CLI interface to GNU Emacs in Python.
Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
SPDX-License-Identifier: GPL-2.0-or-later

python-etask is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

python-etask is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-etask.  If not, see <https://www.gnu.org/licenses/>.
"""


import etask.path


def test_root_dir():

    assert etask.path.dir_path("/")


def test_root_absolute():

    assert etask.path.dir_path("/") == "/"


def test_file_runner():

    assert etask.path.file_path(__file__)
