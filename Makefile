PYTHON          := python3
PIP             := pip
SH              := sh

DOXYGEN         := doxygen
PYTEST          := pytest
TWINE           := twine

FIND            := find
MKDIR           := mkdir -p
RM              := rm -f
RMDIR           := $(RM) -r

PWD             ?= $(shell pwd)
DOCS            := $(PWD)/docs
EXTRAS          := $(PWD)/extras
SCRIPTS         := $(PWD)/scripts
SRC             := $(PWD)/src
TESTS           := $(PWD)/tests

REQUIREMENTS    := $(PWD)/requirements.txt


build-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py build

.PHONY: build
build: build-etask

.PHONY: compile
compile: build


bdist-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py bdist

sdist-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py sdist

wheel-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py bdist_wheel

dist-%:
	$(MAKE) sdist-$(*) wheel-$(*)

.PHONY: dist
dist: dist-etask

.PHONY: pkg
pkg: clean-cache build dist

upload-%:
	$(TWINE) upload $(SRC)/$(*)/dist/*

.PHONY: upload
upload: upload-etask


.PHONY: pip-requirements
pip-requirements:
	$(PIP) install --user -r $(REQUIREMENTS)

pip-install-%:
	$(PIP) install --user -e $(SRC)/$(*)

.PHONY: install
install: pip-install-etask


.PHONY: run
run: compile
run:
	$(PYTHON) $(SCRIPTS)/run.py --debug --gui


pip-remove-%:
	$(PIP) uninstall --yes $(*)

remove: pip-remove-etask


.PHONY: clean-docs
clean-docs:
	$(RMDIR) $(DOCS)/artifacts

docs/artifacts:
	$(MKDIR) $(DOCS)/artifacts
	$(DOXYGEN) $(DOCS)/doxygen.config

.PHONY: docs
docs:
	$(MAKE) clean-docs
	$(MAKE) docs/artifacts

pages: docs/artifacts
pages:
	$(RMDIR) $(PWD)/public
	cp -r $(DOCS)/artifacts/html public

.PHONY: man
man:
	$(SH) $(SCRIPTS)/manpages.sh


.PHONY: test
test:
	cd $(TESTS) && $(PYTEST) --verbose


clean-%:
	$(FIND) $(SRC) -name $(*) -exec $(RMDIR) {} +

.PHONY: clean-cache
clean-cache:
	$(MAKE) clean-__pycache__
	$(MAKE) clean-build
	$(MAKE) clean-dist

clean: clean-docs clean-cache
