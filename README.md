# Python-ETask

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/python-etask">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/python-etask/">
    </a>
    <a href="https://gitlab.com/xgqt/python-etask/pipelines">
        <img src="https://gitlab.com/xgqt/python-etask/badges/master/pipeline.svg">
    </a>
</p>

CLI interface to GNU Emacs.

<p align="center">
    <img src="logo.png">
</p>


## About

ETask is meant to provide a CLI interface exposing some important GNU Emacs
functions that are primarily used in package development and maintenance.
It will not depend on any repository specific configuration files because
it is not meant to be a replacement for Cask, Eldev nor Eask.

So it is better to think of ETask as a command runner,
a small part used for package/repository maintenance,
rather than a whole project manager.


## Installation

### PyPi

Project page: https://pypi.org/project/etask/

``` shell
pip install --user etask
```

### Repository

Repository page: https://gitlab.com/xgqt/python-etask/

``` shell
make clean compile install
```


## Documentation

### Online

Browse ETask documentation online: https://xgqt.gitlab.io/python-etask/

### Building

Doxygen is required to build ETask documentation.

``` shell
make docs
```


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
