#!/bin/sh


# This file is part of python-etask - CLI interface to GNU Emacs in Python.
# Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# python-etask is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# python-etask is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with python-etask.  If not, see <https://www.gnu.org/licenses/>.


trap 'exit 128' INT
set -e
export PATH


myroot="$( realpath "$( dirname "${0}" )/../" )"

cd "${myroot}"


MAJOR=${1}
MINOR=${2}
PATCH=${3}

WHOLE="${MAJOR}.${MINOR}.${PATCH}"

cat <<EOF
MAJOR:  ${MAJOR}
MINOR:  ${MINOR}
PATCH:  ${PATCH}
WHOLE:  ${WHOLE}
EOF

if [ -z "${PATCH}" ] ; then
    echo
    echo "[ERROR] Pass 3 arguments: MAJOR, MINOR and PATCH release numbers"
    exit 1
fi


init_file="src/etask/etask/__init__.py"

sed "/^__version__ = /s|\".*\"|\"${WHOLE}\"|" -i "${init_file}"

git add "${init_file}"
git commit --signoff --message="${init_file}: bump to ${WHOLE}"
git tag "${WHOLE}" --message="bump to ${WHOLE}"
