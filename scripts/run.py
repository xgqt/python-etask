#!/usr/bin/env python3


""""
"""  """

This file is part of python-etask - CLI interface to GNU Emacs in Python.
Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
SPDX-License-Identifier: GPL-2.0-or-later

python-etask is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

python-etask is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-etask.  If not, see <https://www.gnu.org/licenses/>.
"""


import sys
import os


def main():
    """
    Main.
    """

    sys.path.insert(0, os.path.join(
        os.path.dirname(__file__), "..", "src", "etask"))

    try:
        from etask import main as _main
        _main.main()
    except ImportError as exception:
        raise RuntimeError from exception


if __name__ == "__main__":
    main()
